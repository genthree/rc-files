#!/bin/sh
# genthree acpi script

# split arguments on whitepsace
set $*

group="${1%%/*}"
action="${1#*/}"
device="$2"
id="$3"
value="$4"

case "$group" in
	button)
		case "$action" in
			sleep)
				[[ $device == SBTN ]] && echo -n mem > /sys/power/state
			;;

			*)
			;;
		esac
	;;

	*)
	;;
esac
